package com.example.springsecurityrolepermission2.controller;

import com.example.springsecurityrolepermission2.model.Customer;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/customers/")
public class CustomerController {

    @GetMapping("/by-id/{customerId}")
    public Customer getCustomerById(@PathVariable("customerId") Long customerId){
        return CUSTOMERS.stream()
                .filter(customer -> customerId.equals(customer.getId()))
                .findFirst()
                .orElseThrow(()-> new IllegalStateException("Customer " + customerId + " does not exists"));
    }

    @GetMapping("/all")
    public List<Customer> getAllCustomer(){
        return CUSTOMERS;
    }

    @PostMapping
    public String createCustomer(@RequestBody Customer customer){
        customer.setCustomerCode(UUID.randomUUID().toString());
        CUSTOMERS.add(customer);
        System.out.println(CUSTOMERS);
        return "customer created succesfully";
    }

    @PutMapping("/{id}")
    public String updateCustomer(@PathVariable Long id){
        return "UPDATED SUCCESFULLY";
    }

    private static final List<Customer> CUSTOMERS = new ArrayList<>(Arrays.asList(
            new Customer(1L,"Elchin","Akbarov", UUID.randomUUID().toString()),
            new Customer(2L,"Nigar","Rehimova", UUID.randomUUID().toString()),
            new Customer(3L,"Saleh","Asgarov", UUID.randomUUID().toString()),
            new Customer(4L,"Nicat","Bayramov", UUID.randomUUID().toString()),
            new Customer(5L,"Leman","Musayeva", UUID.randomUUID().toString())
    ));
}
