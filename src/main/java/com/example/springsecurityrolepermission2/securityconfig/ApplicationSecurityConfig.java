package com.example.springsecurityrolepermission2.securityconfig;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static com.example.springsecurityrolepermission2.securityconfig.UserPermission.*;
import static com.example.springsecurityrolepermission2.securityconfig.UserRole.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * hasAuthority ye gore admin manager ve customer verilen icazelere uygun endpointlari cagira bilir
     * misal customer in yalniz CUSTOMER_READ  icazesi oldugu ucun cagirdigi endpointlar id e gore ve butun customerleri cagira bilir
     * Managerin hem customer hemde endpointlari gore bildiyine gore hem employee hemde customer daki get metodlarin hamsina accessi var
     * Admin in ise hem get hem post hem put metodlarina employee ve customer deki endpointlara accessi var
     * bu accessleri tebiiki istediyimiz sekilde deyise bilerik
     * misal manager customerde post ve put ede bilsin ancaq employe de buna icaze vermemek kimi
     */
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().disable()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll() // burda her hansisa bir login teleb etmiyecek
                .antMatchers(HttpMethod.GET,"/api/**").hasAuthority(CUSTOMER_READ.getPermission())
                .antMatchers(HttpMethod.POST,"/api/**").hasAuthority(CUSTOMER_WRITE.getPermission())
                .antMatchers(HttpMethod.PUT,"/api/**").hasAuthority(CUSTOMER_WRITE.getPermission())
                .antMatchers(HttpMethod.GET,"/manager/api/**").hasAuthority(EMPLOYEE_READ.getPermission())
                .antMatchers(HttpMethod.POST,"/manager/api/**").hasAuthority(EMPLOYEE_WRITE.getPermission())
                .antMatchers(HttpMethod.PUT,"/manager/api/**").hasAuthority(EMPLOYEE_WRITE.getPermission())
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails elchin = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("el349")
                .password(passwordEncoder.encode("1234"))
                .authorities(ADMIN.getGrantedAuthorities())
                .build();

        UserDetails ilqar = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("ilqar123")
                .password(passwordEncoder.encode("1234"))
                .authorities(MANAGER.getGrantedAuthorities())
                .build();

        UserDetails lale = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("lale123")
                .password(passwordEncoder.encode("1234"))
                .authorities(CUSTOMER.getGrantedAuthorities())
                .build();

        return new InMemoryUserDetailsManager(
                elchin,
                ilqar,
                lale
        );
    }
}
