package com.example.springsecurityrolepermission2.securityconfig;

import com.google.common.collect.Sets;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.example.springsecurityrolepermission2.securityconfig.UserPermission.*;

public enum UserRole {
    CUSTOMER(Sets.newHashSet(CUSTOMER_READ)),
    ADMIN(Sets.newHashSet(CUSTOMER_READ,CUSTOMER_WRITE,EMPLOYEE_READ,EMPLOYEE_WRITE)),
    MANAGER(Sets.newHashSet(CUSTOMER_READ,EMPLOYEE_READ));

    private final Set<UserPermission> permissions;

    UserRole(Set<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<UserPermission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
