package com.example.springsecurityrolepermission2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityRolePermission2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityRolePermission2Application.class, args);
    }

}
